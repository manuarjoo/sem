import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnalysisComponent } from './analysis.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AnalysisComponent]
})
export class AnalysisModule { }
